# Bot vendedor - Etapa técnica Cosmobots

Chegou a hora de por a mão na massa na plataforma!  
Já pensou em criar um bot para vender naves do Starwars? Pode parecer estranho, e é, mas criar este bot é um tanto quanto desafiador!  

## Sobre

O desafio procura entender como você se organizaria para criar um bot de catalogo de vendas.  
Este bot terá:
* Consumo de uma API;
* Tratamento de dados;
* Uso da API interna da Cosmobots para disparo de mensagens;
* Uso da API interna da Cosmobots para gravas e buscar dados do banco de dados;

Seu maior aliado será a nossa [documentação cósmica](https://help.cosmobots.io/help/).

## Requisitos

Utilizando [The Star Wars API](https://swapi.co/) você deve criar um bot que ficará responsável por:
* Exibir um catalogo de Starships disponíveis;
* Permitir que o usuário escolha ao menos uma Starship;
* Seguir como base este [fluxo](https://whimsical.com/9vGCBhDmCpVcACpYwFhrJu);
* **(Opcional)** Menu de naves por filmes, filtrando as naves pelo filme que o usuário escolher;
* **(Opcional)** Permitir que o usuário veja mais detalhes sobre a nave, como por exemplo número de passageiros, quem já pilotou e etc.;
* **(Opcional)** Crie a reserva do pedido no banco de dados do bot;
* **(Opcional)** Liste todas as reservas do usuário;

## Como será avaliado

* Como os fluxos são dividos (Um fluxo deve ter uma responsábilidade);
* Organização das etapas, fluxos e pastas;
* Usabilidade do bot (Ele é claro em suas opções e direciona o usuário de forma lógica);
* Funcionamento dos requisitos;
* Boas práticas de código;

